<?php

$fireBase = "https://angular2-test-project-8e47f.firebaseio.com/";
$nodeName = "invoices.json";
$data = array("uid" => "345", "productName" => "Lamp", "amount" => rand(100, 1000));

$json = json_encode( $data );
$curl = curl_init();
curl_setopt( $curl, CURLOPT_URL, $fireBase . $nodeName );
curl_setopt( $curl, CURLOPT_CUSTOMREQUEST, "POST" );
curl_setopt( $curl, CURLOPT_POSTFIELDS, $json );
curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
$response = curl_exec( $curl );
curl_close( $curl );
$responseParsed = json_decode($response, true);
if($response){ ?>
  <h1>New invoice added:</h1>
  <p><b>Product name:</b><?php echo $data['productName']; ?></p>
  <p><b>Amount:</b><?php echo $data['amount']; ?></p>
  <p><b>Item ID:</b><?php echo $responseParsed['name']; ?></p>
<?php }

?>
