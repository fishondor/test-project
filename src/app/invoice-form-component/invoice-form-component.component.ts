import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';

import { Invoice } from '../invoice/invoice';
import { InvoiceService } from '../invoice.service';

@Component({
  selector: 'app-invoice-form-component',
  templateUrl: './invoice-form-component.component.html',
  styleUrls: ['./invoice-form-component.component.css'],
  providers: [InvoiceService]
})
export class InvoiceFormComponentComponent implements OnInit {

  invoices;
  isLoading: boolean = true;

  constructor(private _InvoiceService: InvoiceService) { }

  invoice: Invoice = {
    name: '',
    amount: 1000
  }

  onSubmit(form:NgForm){
    console.log("Invoice params: " + JSON.stringify(this.invoice));
    this._InvoiceService.addInvoice(this.invoice);
  }

  ngOnInit() {
    this._InvoiceService.getInvoices()
			    .subscribe(invoices => {this.invoices = invoices;
            console.log(invoices);
            
                               this.isLoading = false});
  }

}
