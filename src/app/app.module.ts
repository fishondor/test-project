import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { AngularFireModule } from 'angularfire2';

import { AppComponent } from './app.component';
import { InvoicesComponentComponent } from './invoices-component/invoices-component.component';
import { InvoiceFormComponentComponent } from './invoice-form-component/invoice-form-component.component';
import { ProductComponent } from './product/product.component';
import { InvoiceComponent } from './invoice/invoice.component';

export const firebaseConfig = {
    apiKey: "AIzaSyA2q0JhRSbtUXipBa7zAHfQIMSbY3mEBnc",
    authDomain: "angular2-test-project-8e47f.firebaseapp.com",
    databaseURL: "https://angular2-test-project-8e47f.firebaseio.com",
    storageBucket: "angular2-test-project-8e47f.appspot.com",
    messagingSenderId: "276103206446"
}

const appRoutes: Routes = [
  { path: 'form', component: InvoiceFormComponentComponent },
  { path: 'invoices', component: InvoicesComponentComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    InvoicesComponentComponent,
    InvoiceFormComponentComponent,
    ProductComponent,
    InvoiceComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
