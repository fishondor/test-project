import { Component, OnInit } from '@angular/core';

import { Invoice } from '../invoice/invoice';
import { InvoiceService } from '../invoice.service';

@Component({
  selector: 'app-invoices-component',
  templateUrl: './invoices-component.component.html',
  styleUrls: ['./invoices-component.component.css'],
  providers: [InvoiceService]
})
export class InvoicesComponentComponent implements OnInit {

  invoices;
  isLoading = true;

  constructor(private _InvoiceService: InvoiceService) { }

  ngOnInit() {
    this._InvoiceService.getInvoices()
			    .subscribe(invoices => {this.invoices = invoices;
            console.log(invoices);
            
                               this.isLoading = false});
  }

}
