import { Injectable } from '@angular/core';
import { AngularFire } from 'angularfire2'; 

import 'rxjs/add/operator/map';

@Injectable()
export class InvoiceService {

  invoices = [];

  invoicesObservable;

  constructor(private af:AngularFire) { }

  addInvoice(invoice){
    this.invoicesObservable.push(invoice);
  }

  getInvoices(){
    this.invoicesObservable = this.af.database.list('/invoices').map(
      invoices => {
        invoices.map(
          invoice => {
            
          }
        )
        return invoices;
      }
    );
    
    return this.invoicesObservable;
  }

}
